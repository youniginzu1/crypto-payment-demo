import React from "react";
import { Carousel, Image } from "react-bootstrap";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";

import { getTopProductList } from "../api/product";
import Loader from "./Loader";
import Message from "./Message";

const ProductCarousel = () => {
  const {
    data: topProductData,
    isLoading,
    error,
  } = useQuery(["topProducts"], () => getTopProductList());

  return isLoading ? (
    <Loader />
  ) : error ? (
    <Message variant="danger">{error}</Message>
  ) : (
    <Carousel pause="hover" className="bg-dark">
      {topProductData?.map((product) => (
        <Carousel.Item key={product._id}>
          <Link to={`/product/${product._id}`}>
            <Image src={product.image} alt={product.name} fluid />
            <Carousel.Caption className="carousel-caption">
              <h2>
                {product.name} (${product.price})
              </h2>
            </Carousel.Caption>
          </Link>
        </Carousel.Item>
      ))}
    </Carousel>
  );
};

export default ProductCarousel;
