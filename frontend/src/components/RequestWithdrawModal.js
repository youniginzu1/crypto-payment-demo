import { Modal, Button, Form } from "react-bootstrap";
import React, { useState } from "react";
import { useQueryClient } from "react-query";
import { message } from "antd";

import { createRequestWithdraw } from "../api/request";
import { QueryKey } from "../constants/enum";

export default function RequestWithdrawModal({
  open,
  toggle,
  receiverAddress,
}) {
  const queryClient = useQueryClient();

  const [amount, setAmount] = useState();

  const handleClose = () => {};
  const handleSubmit = (event) => {
    event.preventDefault();
    if (amount && receiverAddress) {
      createRequestWithdraw({
        amount,
        receiverAddress,
      }).then(() => {
        queryClient.invalidateQueries([QueryKey.REQUEST_WITHDRAW_LIST]);
        toggle();
        message.success("Create request success!");
      });
    }
  };

  return (
    <Modal
      show={open}
      onHide={toggle}
      centered
      dialogClassName="request-withdraw-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title>Request withdraw</Modal.Title>
      </Modal.Header>
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="clientId">
          <Form.Label>Amount($)</Form.Label>
          <Form.Control
            type="number"
            step="0.01"
            required
            onChange={(e) => setAmount(Number(e.target.value))}
          ></Form.Control>
        </Form.Group>
        <Modal.Footer>
          <Button variant="secondary" onClick={toggle}>
            Close
          </Button>
          <Button type="submit" variant="primary" onClick={handleClose}>
            Create request
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}
