import React from "react";
import { Container, Nav, NavDropdown, Navbar } from "react-bootstrap";
import { useQueryClient } from "react-query";
import { LinkContainer } from "react-router-bootstrap";
import { Route } from "react-router-dom";

import { CookieKey, QueryKey } from "../constants/enum";
import cookie from "../utils/helper/cookie";
import useProfile from "../utils/hooks/useProfile";
import SearchBox from "./SearchBox";

const Header = () => {
  const queryClient = useQueryClient();

  const isAuthenticated = !!cookie.getItem(CookieKey.TOKEN);
  const { profile } = useProfile(isAuthenticated);

  const logoutHandler = () => {
    cookie.removeItem(CookieKey.TOKEN);
    queryClient.setQueryData([QueryKey.PROFILE], () => null);
  };

  return (
    <header>
      <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect>
        <Container>
          <LinkContainer to="/">
            <Navbar.Brand>ProShop</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Route render={({ history }) => <SearchBox history={history} />} />
            <Nav className="ml-auto">
              <LinkContainer to="/cart">
                <Nav.Link>
                  <i className="fas fa-shopping-cart"></i> Cart
                </Nav.Link>
              </LinkContainer>
              {profile ? (
                <NavDropdown title={profile?.name} id="username">
                  <LinkContainer to="/profile">
                    <NavDropdown.Item>Profile</NavDropdown.Item>
                  </LinkContainer>
                  <LinkContainer to="/request-withdraw">
                    <NavDropdown.Item>Request withdraw</NavDropdown.Item>
                  </LinkContainer>
                  <LinkContainer to="/transaction-histories">
                    <NavDropdown.Item>Transaction histories</NavDropdown.Item>
                  </LinkContainer>
                  <NavDropdown.Item onClick={logoutHandler}>
                    Logout
                  </NavDropdown.Item>
                </NavDropdown>
              ) : (
                <LinkContainer to="/login">
                  <Nav.Link>
                    <i className="fas fa-user"></i> Sign In
                  </Nav.Link>
                </LinkContainer>
              )}
              {profile && profile?.isAdmin && (
                <NavDropdown title="Admin" id="adminmenu">
                  <LinkContainer to="/admin/userlist">
                    <NavDropdown.Item>Users</NavDropdown.Item>
                  </LinkContainer>
                  <LinkContainer to="/admin/productlist">
                    <NavDropdown.Item>Products</NavDropdown.Item>
                  </LinkContainer>
                  <LinkContainer to="/admin/orderlist">
                    <NavDropdown.Item>Orders</NavDropdown.Item>
                  </LinkContainer>
                </NavDropdown>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
