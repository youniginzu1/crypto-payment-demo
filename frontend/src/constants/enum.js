export const QueryKey = {
  CART_ITEMS: "cart-items",
  SHIPPING_ADDRESS: "shipping-address",
  PAYMENT_METHOD: "payment-method",
  PRODUCT_DETAIL: "product-detail",
  PRODUCT_LIST: "product-list",
  TRANSACTION_HISTORY_LIST: "transaction-history-list",
  USER_DETAIL: "user-detail",
  USER_LIST: "user-list",
  PROFILE: "profile",
  REQUEST_WITHDRAW_LIST: "request-draw-list",
};

export const LocalStorageKey = {
  CART_ITEMS: "cart-items",
  SHIPPING_ADDRESS: "shipping-address",
  PAYMENT_METHOD: "payment-method",
};

export const CookieKey = {
  TOKEN: "access-token",
};
