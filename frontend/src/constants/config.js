const configs = {
  API_DOMAIN: process.env.REACT_APP_API_DOMAIN,
  APP_ENV: process.env.REACT_APP_APP_ENV,
  PAYMENT_URL: process.env.REACT_APP_PAYMENT_URL,
};

export default configs;
