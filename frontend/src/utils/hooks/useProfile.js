import { useQuery } from "react-query";

import { getUserDetail } from "../../api/user";
import { QueryKey } from "../../constants/enum";

export default function useProfile(enabled = false) {
  const {
    data: profile,
    refetch: refetchProfile,
    isLoading: loadingProfile,
    isFetching: fetchingProfile,
    error,
  } = useQuery(
    [QueryKey.PROFILE],
    async () => {
      const profile = await getUserDetail(QueryKey.PROFILE);
      return profile;
    },
    {
      enabled,
      keepPreviousData: true,
    }
  );
  return { profile, refetchProfile, loadingProfile, fetchingProfile, error };
}
