import { useCallback } from "react";
import { useQuery, useQueryClient } from "react-query";

import { LocalStorageKey, QueryKey } from "../../constants/enum";

export default function useCart() {
  const queryClient = useQueryClient();

  const { data: cartItems } = useQuery([QueryKey.CART_ITEMS], () => {
    return (
      queryClient.getQueryData([QueryKey.CART_ITEMS]) ||
      JSON.parse(localStorage.getItem(LocalStorageKey.CART_ITEMS) || "[]")
    );
  });
  const { data: shippingAddress } = useQuery(
    [QueryKey.SHIPPING_ADDRESS],
    () => {
      return (
        queryClient.getQueryData([QueryKey.SHIPPING_ADDRESS]) ||
        JSON.parse(
          localStorage.getItem(LocalStorageKey.SHIPPING_ADDRESS) || "null"
        )
      );
    }
  );
  const { data: paymentMethod } = useQuery([QueryKey.PAYMENT_METHOD], () => {
    return (
      queryClient.getQueryData([QueryKey.PAYMENT_METHOD]) ||
      JSON.parse(localStorage.getItem(LocalStorageKey.PAYMENT_METHOD) || "null")
    );
  });

  const saveCartItemsData = useCallback((dataSave) => {
    queryClient.setQueryData([QueryKey.CART_ITEMS], () => dataSave);
    localStorage.setItem(LocalStorageKey.CART_ITEMS, JSON.stringify(dataSave));
  }, []);
  const setCartItems = useCallback(
    (data) => {
      saveCartItemsData(data);
    },
    [queryClient]
  );
  const addToCart = useCallback(
    (product, qty) => {
      const newItem = {
        product: product?._id,
        name: product?.name,
        image: product?.image,
        price: product?.price,
        countInStock: product?.countInStock,
        qty,
      };
      const existItem = cartItems?.find(
        (item) => item?.product === product?._id
      );
      let dataSave;

      if (existItem) {
        dataSave = cartItems?.map((item) =>
          item?.product === existItem?.product ? newItem : item
        );
      } else {
        dataSave = [...cartItems, newItem];
      }

      saveCartItemsData(dataSave);
    },
    [queryClient, cartItems]
  );
  const removeFromCart = useCallback(
    (id) => {
      const dataSave = cartItems?.filter((item) => item?.product !== id);
      saveCartItemsData(dataSave);
    },
    [queryClient, cartItems]
  );
  const removeAllItemsInCart = useCallback(() => {
    saveCartItemsData([]);
  }, [queryClient]);
  const saveShippingAddress = useCallback(
    (data) => {
      queryClient.setQueryData([QueryKey.SHIPPING_ADDRESS], () => data);
      localStorage.setItem(
        LocalStorageKey.SHIPPING_ADDRESS,
        JSON.stringify(data)
      );
    },
    [queryClient]
  );
  const savePaymentMethod = useCallback(
    (data) => {
      queryClient.setQueryData([QueryKey.PAYMENT_METHOD], () => data);
      localStorage.setItem(
        LocalStorageKey.PAYMENT_METHOD,
        JSON.stringify(data)
      );
    },
    [queryClient]
  );

  return {
    cartItems,
    shippingAddress,
    paymentMethod,
    setCartItems,
    addToCart,
    removeFromCart,
    removeAllItemsInCart,
    saveShippingAddress,
    savePaymentMethod,
  };
}
