const setItem = (key, value, type) => {
  if (type === "local-storage") {
    localStorage.setItem(key, JSON.stringify(value));
  }
  if (type === "session-storage") {
    sessionStorage.setItem(key, JSON.stringify(value));
  }
};

const getItem = (key, type) => {
  if (type === "local-storage") {
    return JSON.parse(localStorage.getItem(key) || "null");
  }
  if (type === "session-storage") {
    return JSON.parse(sessionStorage.getItem(key) || "null");
  }
};

const removeItem = (key, type) => {
  if (type === "local-storage") {
    localStorage.removeItem(key);
  }
  if (type === "session-storage") {
    sessionStorage.removeItem(key);
  }
};

const storage = {
  setItem,
  getItem,
  removeItem,
};
export default storage;
