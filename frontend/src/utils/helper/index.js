import moment from "moment";
import { message } from "antd";

import configs from "../../constants/config";

export const handleErrorMessage = (error) => {
  message.destroy();
  message.error(getErrorMessage(error));
  if (configs.APP_ENV !== "prod") {
    console.log(error);
  }
};

export const getErrorMessage = (error) => {
  return (
    error?.response?.data?.message || error?.message || "Something went wrong!"
  );
};

export const formatDate = (date) => {
  return moment(date).format("YYYY-MM-DD HH:mm:ss");
};

export const addDecimals = (num) => {
  return (Math.round(num * 100) / 100).toFixed(2);
};

export const getRequestWithdrawStatusText = (status) => {
  if (status === 1) {
    return "Pending";
  }
  if (status === 2) {
    return "Success";
  }
  if (status === 3) {
    return "Failed";
  }
  return "";
};
