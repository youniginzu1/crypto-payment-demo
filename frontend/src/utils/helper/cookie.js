import Cookies from "js-cookie";

const EXPIRES = 1;

const setCookieData = (key, value) => {
  Cookies.set(key, value, {
    expires: EXPIRES,
    path: "/",
  });
};

const getCookieData = (key) => {
  return Cookies.get(key);
};

const clearCookieData = (key) => {
  Cookies.remove(key, { path: "/" });
};

const setItem = (key, value) => {
  setCookieData(key, JSON.stringify(value));
};

const getItem = (key) => {
  return JSON.parse(getCookieData(key) || "null");
};

const removeItem = (key) => {
  clearCookieData(key);
};

const cookie = {
  setItem,
  getItem,
  removeItem,
};
export default cookie;
