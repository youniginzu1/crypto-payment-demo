import { axiosInstance } from "./axios";

import {
  REQUEST_CREATE_FAIL,
  REQUEST_CREATE_REQUEST,
  REQUEST_CREATE_SUCCESS,
} from "../constants/requestConstants";
import configs from "../constants/config";

export const createRequest = (amount, receiverAddress) => async (dispatch) => {
  try {
    dispatch({
      type: REQUEST_CREATE_REQUEST,
    });

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const { data } = await axiosInstance.post(
      `${configs.PAYMENT_API_URL}/request/withdraw`,
      { amount, receiverAddress },
      config
    );

    dispatch({
      type: REQUEST_CREATE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: REQUEST_CREATE_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
