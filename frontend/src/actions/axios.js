import Axios from "axios";

import configs from "../constants/config";

export const axiosInstance = Axios.create({
  timeout: 3 * 60 * 1000,
  baseURL: configs.API_DOMAIN,
});
