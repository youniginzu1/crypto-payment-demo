import {
  TRANSACTION_HISTORY_LIST_MY_FAIL,
  TRANSACTION_HISTORY_LIST_MY_REQUEST,
  TRANSACTION_HISTORY_LIST_MY_SUCCESS,
} from "../constants/transactionHistoryConstants";
import { axiosInstance } from "./axios";

export const listMyTransactionHistory = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: TRANSACTION_HISTORY_LIST_MY_REQUEST,
    });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axiosInstance.get(
      `/api/transaction-histories`,
      config
    );

    dispatch({
      type: TRANSACTION_HISTORY_LIST_MY_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: TRANSACTION_HISTORY_LIST_MY_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
