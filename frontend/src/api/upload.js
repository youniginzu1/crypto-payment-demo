import { sendPost } from "./axios";

export const uploadFile = (file) => {
  const formData = new FormData();
  formData.append("image", file);
  return sendPost("/api/upload", formData);
};
