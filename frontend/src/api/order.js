import { sendGet, sendPost, sendPut } from "./axios";

export const createOrder = (payload) => sendPost("/api/orders", payload);
export const getOrderDetail = (id) => sendGet(`/api/orders/${id}`);
export const payOrder = (id, payload) =>
  sendPut(`/api/orders/${id}/pay`, payload);
export const deliverOrder = (id) => sendPut(`/api/orders/${id}/deliver`);
export const getMyOrderList = () => sendGet("/api/orders/myorders");
export const getOrderList = () => sendGet("/api/orders");
