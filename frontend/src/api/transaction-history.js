import { sendGet } from "./axios";

export const getMyTransactionHistoryList = () =>
  sendGet(`/api/transaction-histories`);
