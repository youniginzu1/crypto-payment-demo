import { sendDelete, sendGet, sendPost, sendPut } from "./axios";

export const getProductList = (params) => sendGet("/api/products", params);
export const getProductDetail = (id) => sendGet(`/api/products/${id}`);
export const deleteProduct = (id) => sendDelete(`/api/products/${id}`);
export const updateProduct = (id, payload) =>
  sendPut(`/api/products/${id}`, payload);
export const createProduct = (payload) => sendPost("/api/products", payload);
export const createProductPreview = (id, payload) =>
  sendPost(`/api/products/${id}/reviews`, payload);
export const getTopProductList = () => sendGet("/api/products/top");
