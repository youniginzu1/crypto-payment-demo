import { sendGet, sendPost } from "./axios";

export const createRequestWithdraw = (payload) =>
  sendPost(`/api/request-withdraw`, payload);
export const getRequestWithdrawList = () =>
  sendGet("/api/request-withdraw/list");
