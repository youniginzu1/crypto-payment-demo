import { sendGet, sendPost, sendPut, sendDelete } from "./axios";

export const loginApi = (payload) => sendPost("/api/users/login", payload);
export const register = (payload) => sendPost("/api/users", payload);
export const getUserDetail = (id) => sendGet(`/api/users/${id}`);
export const updateProfile = (payload) =>
  sendPut("/api/users/profile", payload);
export const getUserList = () => sendGet("/api/users");
export const deleteUser = (id) => sendDelete(`/api/users/${id}`);
export const updateUser = (id, payload) => sendPut(`/api/users/${id}`, payload);
