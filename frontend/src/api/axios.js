import Axios from "axios";

import configs from "../constants/config";
import { handleErrorMessage } from "../utils/helper";
import cookie from "../utils/helper/cookie";
import { CookieKey } from "../constants/enum";

const axiosInstance = Axios.create({
  timeout: 3 * 60 * 1000,
  baseURL: configs.API_DOMAIN,
});
axiosInstance.interceptors.request.use(
  (config) => {
    const token = cookie.getItem(CookieKey.TOKEN);
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) => {
    const originalConfig = error.config;
    if (error.response?.status !== 401) {
      handleErrorMessage(error);
      return Promise.reject(error);
    }
    return Axios.post(`${configs.API_DOMAIN}/auth/refresh-token`, {
      refreshToken: cookie.getItem("refreshToken"),
    })
      .then((res) => {
        if (res?.data?.data?.token) {
          const token = res.data.data.token;
          cookie.setItem(CookieKey.TOKEN, token);
          originalConfig.headers.Authorization = `Bearer ${token}`;
          return Axios(originalConfig);
        }
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  }
);

export const sendGet = (url, params) =>
  axiosInstance.get(url, { params }).then((res) => res.data);
export const sendPost = (url, params, queryParams) =>
  axiosInstance
    .post(url, params, { params: queryParams })
    .then((res) => res.data);
export const sendPut = (url, params) =>
  axiosInstance.put(url, params).then((res) => res.data);
export const sendPatch = (url, params) =>
  axiosInstance.patch(url, params).then((res) => res.data);
export const sendDelete = (url, params) =>
  axiosInstance.delete(url, { params }).then((res) => res.data);
