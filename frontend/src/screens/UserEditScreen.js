import React, { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";

import { getUserDetail, updateUser } from "../api/user";
import FormContainer from "../components/FormContainer";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { QueryKey } from "../constants/enum";

const UserEditScreen = ({ match, history }) => {
  const userId = match.params.id;

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [isAdmin, setIsAdmin] = useState(false);
  const [loadingPage, setLoadingPage] = useState(false);

  const {
    data: userDetail,
    isLoading: userDetailLoading,
    error: userDetailError,
  } = useQuery([QueryKey.USER_DETAIL], () => getUserDetail(userId), {
    enabled: !!userId,
  });

  useEffect(() => {
    if (userDetail) {
      setName(userDetail.name);
      setEmail(userDetail.email);
      setIsAdmin(userDetail.isAdmin);
    }
  }, [userDetail]);

  const submitHandler = (e) => {
    e.preventDefault();
    setLoadingPage(true);
    updateUser({ _id: userId, name, email, isAdmin })
      .then(() => {
        history.push("/admin/userlist");
      })
      .finally(() => {
        setLoadingPage(false);
      });
  };

  return (
    <>
      <Link to="/admin/userlist" className="btn btn-light my-3">
        Go Back
      </Link>
      <FormContainer>
        <h1>Edit User</h1>
        {loadingPage && <Loader />}
        {userDetailLoading ? (
          <Loader />
        ) : userDetailError ? (
          <Message variant="danger">{userDetailError}</Message>
        ) : (
          <Form onSubmit={submitHandler}>
            <Form.Group controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="name"
                placeholder="Enter name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId="email">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId="isadmin">
              <Form.Check
                type="checkbox"
                label="Is Admin"
                checked={isAdmin}
                onChange={(e) => setIsAdmin(e.target.checked)}
              ></Form.Check>
            </Form.Group>

            <Button type="submit" variant="primary">
              Update
            </Button>
          </Form>
        )}
      </FormContainer>
    </>
  );
};

export default UserEditScreen;
