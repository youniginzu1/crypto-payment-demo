import React, { useEffect, useState } from "react";
import { Button, Col, Form, Row, Table } from "react-bootstrap";
import { useQuery } from "react-query";

import { getRequestWithdrawList } from "../api/request";
import Loader from "../components/Loader";
import Message from "../components/Message";
import RequestWithdrawModal from "../components/RequestWithdrawModal";
import { QueryKey } from "../constants/enum";
import { formatDate, getRequestWithdrawStatusText } from "../utils/helper";
import useProfile from "../utils/hooks/useProfile";

const RequestWithdrawScreen = ({ location, history }) => {
  const [walletAddress, setWalletAddress] = useState("");
  const [openRequestWithdrawModal, setOpenRequestWithdrawModal] =
    useState(false);

  const { profile } = useProfile();
  const {
    data: requestWithdrawList,
    isLoading,
    error,
  } = useQuery([QueryKey.REQUEST_WITHDRAW_LIST], () =>
    getRequestWithdrawList()
  );

  useEffect(() => {
    if (profile) {
      setWalletAddress(profile?.walletAddress);
    }
  }, [profile]);

  const submitHandler = (e) => {
    e.preventDefault();
  };
  const getStatusEl = (status) => {
    if (status === 2) {
      return <span className="pending">Pending</span>;
    }
    if (status === 1) {
      return <span className="success">Success</span>;
    }
    if (status === 0) {
      return <span className="fail">Fail</span>;
    }
  };

  return (
    <Row>
      <Col md={3}>
        <h2>Wallet Info</h2>
        <p className="line-1" title={walletAddress}>
          {walletAddress}
        </p>
        <Form onSubmit={submitHandler}>
          <Form.Group controlId="clientId">
            <Form.Label>Bonus point($)</Form.Label>
            <Form.Control
              type="bonusPoint"
              value={profile?.bonusPoint?.toFixed(2) || 0}
              disabled
            ></Form.Control>
          </Form.Group>

          <Button
            type="submit"
            variant="primary"
            onClick={() => setOpenRequestWithdrawModal(true)}
          >
            Withdraw
          </Button>
        </Form>
      </Col>
      <Col md={9}>
        <h2>My request withdraw</h2>
        {isLoading ? (
          <Loader />
        ) : error ? (
          <Message variant="danger">{error}</Message>
        ) : (
          <Table striped bordered hover responsive className="table-sm">
            <thead>
              <tr>
                <th>RECEIVER ADDRESS</th>
                <th>AMOUNT</th>
                <th>STATUS</th>
                <th>CREATED AT</th>
                <th>UPDATED AT</th>
              </tr>
            </thead>
            <tbody>
              {requestWithdrawList?.map((rw) => (
                <tr key={rw._id}>
                  <td>{rw?.receiverAddress}</td>
                  <td>${rw?.amount?.toFixed(2)}</td>
                  <td>{getStatusEl(rw?.status)}</td>
                  <td>{formatDate(rw?.createdAt)}</td>
                  <td>{formatDate(rw?.updatedAt)}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      </Col>
      {openRequestWithdrawModal && (
        <RequestWithdrawModal
          open={openRequestWithdrawModal}
          toggle={() => setOpenRequestWithdrawModal(!openRequestWithdrawModal)}
          receiverAddress={walletAddress}
        />
      )}
    </Row>
  );
};

export default RequestWithdrawScreen;
