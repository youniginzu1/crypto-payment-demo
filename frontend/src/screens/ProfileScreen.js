import { message } from "antd";
import React, { useEffect, useState } from "react";
import { Button, Col, Form, Row, Table } from "react-bootstrap";
import { useQuery } from "react-query";

import { getMyOrderList } from "../api/order";
import { updateProfile } from "../api/user";
import Loader from "../components/Loader";
import Message from "../components/Message";
import useProfile from "../utils/hooks/useProfile";
import { Link } from "react-router-dom";

const ProfileScreen = ({ location, history }) => {
  const [name, setName] = useState("");
  const [clientId, setClientId] = useState("");
  const [email, setEmail] = useState("");
  const [walletAddress, setWalletAddress] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [loadingPage, setLoadingPage] = useState(false);

  const { profile } = useProfile();
  const {
    data: myOrderData,
    isLoading: loadingMyOrder,
    error: errorMyOrder,
  } = useQuery(["myOrderList"], () => getMyOrderList());

  useEffect(() => {
    if (profile) {
      setName(profile?.name);
      setEmail(profile?.email);
      setClientId(profile?._id);
      setWalletAddress(profile?.walletAddress);
    }
  }, [profile]);

  const submitHandler = (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      message.warning("Passwords do not match.");
      return;
    }
    if (password === confirmPassword) {
      setLoadingPage(true);
      updateProfile({
        id: profile?._id,
        name,
        email,
        password,
        walletAddress,
      })
        .then(() => {
          message.success("Profile updated.");
        })
        .finally(() => {
          setLoadingPage(false);
        });
    }
  };

  return (
    <Row>
      <Col md={3}>
        <h2>User Profile</h2>
        {loadingPage && <Loader />}
        <Form onSubmit={submitHandler}>
          <Form.Group controlId="clientId">
            <Form.Label>Client id</Form.Label>
            <Form.Control
              type="clientId"
              placeholder="Client id"
              value={clientId}
              disabled
            ></Form.Control>
          </Form.Group>

          <Form.Group controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="name"
              placeholder="Enter name"
              value={name}
              onChange={(e) => setName(e.target.value)}
            ></Form.Control>
          </Form.Group>

          <Form.Group controlId="email">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            ></Form.Control>
          </Form.Group>

          <Form.Group controlId="walletAdress">
            <Form.Label>Wallet Address</Form.Label>
            <Form.Control
              type="walletAddress"
              placeholder="Enter wallet address"
              value={walletAddress}
              onChange={(e) => setWalletAddress(e.target.value)}
            ></Form.Control>
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Password Address</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            ></Form.Control>
          </Form.Group>

          <Form.Group controlId="confirmPassword">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Confirm password"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            ></Form.Control>
          </Form.Group>

          <Button type="submit" variant="primary">
            Update
          </Button>
        </Form>
      </Col>
      <Col md={9}>
        <h2>My Orders</h2>
        {loadingMyOrder ? (
          <Loader />
        ) : errorMyOrder ? (
          <Message variant="danger">{errorMyOrder}</Message>
        ) : (
          <Table striped bordered hover responsive className="table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>DATE</th>
                <th>TOTAL</th>
                <th>PAID</th>
                <th>DELIVERED</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {myOrderData.map((order) => (
                <tr key={order._id}>
                  <td>{order._id}</td>
                  <td>{order.createdAt.substring(0, 10)}</td>
                  <td>{order.totalPrice}</td>
                  <td>
                    {order?.isPaid ? (
                      order?.transactionHistory?.paidAt?.substring(0, 10)
                    ) : (
                      <i className="fas fa-times" style={{ color: "red" }}></i>
                    )}
                  </td>
                  <td>
                    {order.isDelivered ? (
                      order.deliveredAt.substring(0, 10)
                    ) : (
                      <i className="fas fa-times" style={{ color: "red" }}></i>
                    )}
                  </td>
                  <td>
                    <Link to={`/order/${order._id}`}>
                      <Button className="btn-sm" variant="light">
                        Details
                      </Button>
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      </Col>
    </Row>
  );
};

export default ProfileScreen;
