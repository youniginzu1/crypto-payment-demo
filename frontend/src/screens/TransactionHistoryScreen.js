import React from "react";
import { Col, Row, Table } from "react-bootstrap";
import { useQuery } from "react-query";

import { getMyTransactionHistoryList } from "../api/transaction-history";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { QueryKey } from "../constants/enum";
import { formatDate } from "../utils/helper";

const TransactionHistoryScreen = ({ history, match }) => {
  const {
    data: transactionHistoryList,
    isLoading,
    error,
  } = useQuery([QueryKey.TRANSACTION_HISTORY_LIST], () =>
    getMyTransactionHistoryList()
  );

  const getTransactionHistoryTypeEl = (th) => {
    if (th?.type === 1) {
      return (
        <>
          <td>{"BUY PRODUCT"}</td>
          <td className="outcome">{th?.amount?.toFixed(2) + "$"}</td>
        </>
      );
    }
    if (th?.type === 2) {
      return (
        <>
          <td>{"WITHDRAW"}</td>
          <td className="income">{th?.amount?.toFixed(2) + "$"}</td>
        </>
      );
    }
  };

  return (
    <>
      <Row className="align-items-center">
        <Col>
          <h1>Transaction histories</h1>
        </Col>
      </Row>
      {isLoading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <>
          <Table striped bordered hover responsive className="table-sm">
            <thead>
              <tr>
                <th>METHOD</th>
                <th>TYPE</th>
                <th>POINT</th>
                <th>BONUS POINT</th>
                <th>PAID AT</th>
              </tr>
            </thead>
            <tbody>
              {transactionHistoryList?.map((th) => (
                <tr key={th._id}>
                  <td>{th?.paymentMethod}</td>
                  {getTransactionHistoryTypeEl(th)}
                  <td>{th?.bonusPoint?.toFixed(2) + "$"}</td>
                  <td>{formatDate(th?.paidAt)}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </>
      )}
    </>
  );
};

export default TransactionHistoryScreen;
