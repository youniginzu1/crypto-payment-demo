import { message } from "antd";
import React, { useState } from "react";
import { Button, Table } from "react-bootstrap";
import { useQuery } from "react-query";

import { deleteUser, getUserList } from "../api/user";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { QueryKey } from "../constants/enum";
import { Link } from "react-router-dom";

const UserListScreen = ({ history }) => {
  const [loadingPage, setLoadingPage] = useState(false);

  const {
    data: userList,
    isLoading,
    error,
  } = useQuery([QueryKey.USER_LIST], () => getUserList());

  const deleteHandler = (id) => {
    if (window.confirm("Are you sure")) {
      setLoadingPage(true);
      deleteUser(id)
        .then(() => {
          message.success("Delete user success!");
        })
        .finally(() => {
          setLoadingPage(false);
        });
    }
  };

  return (
    <>
      <h1>Users</h1>
      {isLoading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <Table striped bordered hover responsive className="table-sm">
          <thead>
            <tr>
              <th>ID</th>
              <th>NAME</th>
              <th>EMAIL</th>
              <th>ADMIN</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {userList?.map((user) => (
              <tr key={user._id}>
                <td>{user._id}</td>
                <td>{user.name}</td>
                <td>
                  <a href={`mailto:${user.email}`}>{user.email}</a>
                </td>
                <td>
                  {user.isAdmin ? (
                    <i className="fas fa-check" style={{ color: "green" }}></i>
                  ) : (
                    <i className="fas fa-times" style={{ color: "red" }}></i>
                  )}
                </td>
                <td>
                  <Link to={`/admin/user/${user._id}/edit`}>
                    <Button variant="light" className="btn-sm">
                      <i className="fas fa-edit"></i>
                    </Button>
                  </Link>
                  <Button
                    variant="danger"
                    className="btn-sm"
                    onClick={() => deleteHandler(user._id)}
                  >
                    <i className="fas fa-trash"></i>
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </>
  );
};

export default UserListScreen;
