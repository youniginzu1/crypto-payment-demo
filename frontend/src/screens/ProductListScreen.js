import React, { useState } from "react";
import { Button, Col, Row, Table } from "react-bootstrap";
import { useQuery } from "react-query";

import { deleteProduct, getProductList } from "../api/product";
import Loader from "../components/Loader";
import Message from "../components/Message";
import Paginate from "../components/Paginate";
import { QueryKey } from "../constants/enum";
import useProfile from "../utils/hooks/useProfile";
import { Link } from "react-router-dom";

const ProductListScreen = ({ history, match }) => {
  const pageNumber = match.params.pageNumber || 1;

  const [loadingPage, setLoadingPage] = useState(false);

  const {
    data: productList,
    isLoading: productListLoading,
    error: productListError,
  } = useQuery([QueryKey.PRODUCT_LIST], () =>
    getProductList({
      keyword: "",
      pageNumber,
    })
  );
  const { profile } = useProfile();

  // useEffect(() => {
  //   dispatch({ type: PRODUCT_CREATE_RESET });

  //   if (!userInfo || !userInfo.isAdmin) {
  //     history.push("/login");
  //   }

  //   if (successCreate) {
  //     history.push(`/admin/product/${createdProduct._id}/edit`);
  //   } else {
  //     dispatch(listProducts("", pageNumber));
  //   }
  // }, [
  //   dispatch,
  //   history,
  //   userInfo,
  //   successDelete,
  //   successCreate,
  //   createdProduct,
  //   pageNumber,
  // ]);

  const deleteHandler = (id) => {
    if (window.confirm("Are you sure")) {
      setLoadingPage(true);
      deleteProduct(id)
        .then(() => {})
        .finally(() => {
          setLoadingPage(false);
        });
    }
  };

  const createProductHandler = () => {};

  return (
    <>
      <Row className="align-items-center">
        <Col>
          <h1>Products</h1>
        </Col>
        <Col className="text-right">
          <Button className="my-3" onClick={createProductHandler}>
            <i className="fas fa-plus"></i> Create Product
          </Button>
        </Col>
      </Row>
      {loadingPage && <Loader />}
      {productListLoading ? (
        <Loader />
      ) : productListError ? (
        <Message variant="danger">{productListError}</Message>
      ) : (
        <>
          <Table striped bordered hover responsive className="table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>PRICE</th>
                <th>CATEGORY</th>
                <th>BRAND</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {productList?.products?.map((product) => (
                <tr key={product._id}>
                  <td>{product._id}</td>
                  <td>{product.name}</td>
                  <td>${product.price}</td>
                  <td>{product.category}</td>
                  <td>{product.brand}</td>
                  <td>
                    <Link to={`/admin/product/${product._id}/edit`}>
                      <Button variant="light" className="btn-sm">
                        <i className="fas fa-edit"></i>
                      </Button>
                    </Link>
                    <Button
                      variant="danger"
                      className="btn-sm"
                      onClick={() => deleteHandler(product._id)}
                    >
                      <i className="fas fa-trash"></i>
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Paginate
            pages={productList?.pages}
            page={productList?.page}
            isAdmin={true}
          />
        </>
      )}
    </>
  );
};

export default ProductListScreen;
