import React, { useEffect, useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

import { message } from "antd";
import { register } from "../api/user";
import FormContainer from "../components/FormContainer";
import Loader from "../components/Loader";
import useProfile from "../utils/hooks/useProfile";

const RegisterScreen = ({ location, history }) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [loadingPage, setLoadingPage] = useState(false);

  const { profile } = useProfile();

  const redirect = location.search ? location.search.split("=")[1] : "/";

  useEffect(() => {
    if (profile) {
      history.push(redirect);
    }
  }, [profile]);

  const submitHandler = (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      message.warning("Passwords do not match");
    } else {
      setLoadingPage(true);
      register({
        name,
        email,
        password,
      })
        .then(() => {
          message.success("Register success!");
        })
        .finally(() => {
          setLoadingPage(false);
        });
    }
  };

  return (
    <FormContainer>
      <h1>Sign Up</h1>
      {loadingPage && <Loader />}
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="name"
            placeholder="Enter name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Form.Group controlId="email">
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password Address</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Form.Group controlId="confirmPassword">
          <Form.Label>Confirm Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Confirm password"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Button type="submit" variant="primary">
          Register
        </Button>
      </Form>

      <Row className="py-3">
        <Col>
          Have an Account?{" "}
          <Link to={redirect ? `/login?redirect=${redirect}` : "/login"}>
            Login
          </Link>
        </Col>
      </Row>
    </FormContainer>
  );
};

export default RegisterScreen;
