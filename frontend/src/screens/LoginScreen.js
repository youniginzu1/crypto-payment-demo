import React, { useEffect, useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useQueryClient } from "react-query";

import { loginApi } from "../api/user";
import FormContainer from "../components/FormContainer";
import Loader from "../components/Loader";
import cookie from "../utils/helper/cookie";
import useProfile from "../utils/hooks/useProfile";
import { CookieKey, QueryKey } from "../constants/enum";

const LoginScreen = ({ location, history }) => {
  const queryClient = useQueryClient();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loadingPage, setLoadingPage] = useState(false);

  const { profile } = useProfile();
  const redirect = location.search ? location.search.split("=")[1] : "/";

  useEffect(() => {
    if (profile) {
      history.push(redirect);
    }
  }, [history, profile, redirect]);

  const submitHandler = (e) => {
    e.preventDefault();
    if (email && password) {
      setLoadingPage(true);
      loginApi({
        email,
        password,
      })
        .then((data) => {
          const token = data?.token;
          if (token) {
            cookie.setItem(CookieKey.TOKEN, token);
          }
          queryClient.setQueryData([QueryKey.PROFILE], () => data);
        })
        .finally(() => {
          setLoadingPage(false);
        });
    }
  };

  return (
    <FormContainer>
      <h1>Sign In</h1>
      {loadingPage && <Loader />}
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="email">
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password Address</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Button type="submit" variant="primary">
          Sign In
        </Button>
      </Form>

      <Row className="py-3">
        <Col>
          New Customer?{" "}
          <Link to={redirect ? `/register?redirect=${redirect}` : "/register"}>
            Register
          </Link>
        </Col>
      </Row>
    </FormContainer>
  );
};

export default LoginScreen;
