import React from "react";
import { Button, Card, Col, Image, ListGroup, Row } from "react-bootstrap";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";

import { getOrderDetail, deliverOrder } from "../api/order";
import Loader from "../components/Loader";
import Message from "../components/Message";
import configs from "../constants/config";
import { formatDate } from "../utils/helper";
import useProfile from "../utils/hooks/useProfile";

const OrderScreen = ({ match, history }) => {
  const orderId = match.params.id;

  const { profile } = useProfile();
  const {
    data: orderDetail,
    isLoading,
    error,
  } = useQuery(
    ["order-detail", orderId],
    async () => {
      const orderDetailData = await getOrderDetail(orderId);
      return {
        ...orderDetailData,
        itemsPrice: orderDetailData?.orderItems?.reduce(
          (acc, item) => acc + item.price * item.qty,
          0
        ),
      };
    },
    {
      enabled: !!orderId,
    }
  );

  const handleCheckOut = () => {
    const orderId = orderDetail?._id;
    const totalPrice = orderDetail?.totalPrice;
    const clientId = orderDetail?.user?._id;

    if (orderId && totalPrice && clientId) {
      window.location.href = `${configs.PAYMENT_URL}/payment?price=${totalPrice}&client_id=${clientId}&order_id=${orderId}&callback_url=${window.location.href}`;
    }
  };

  const deliverHandler = () => {
    deliverOrder(orderDetail);
  };

  return isLoading ? (
    <Loader />
  ) : error ? (
    <Message variant="danger">{error}</Message>
  ) : (
    <>
      <h1>Order {orderDetail?._id}</h1>
      <Row>
        <Col md={8}>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <h2>Shipping</h2>
              <p>
                <strong>Name: </strong> {orderDetail.user.name}
              </p>
              <p>
                <strong>Email: </strong>{" "}
                <a href={`mailto:${orderDetail.user.email}`}>
                  {orderDetail.user.email}
                </a>
              </p>
              <p>
                <strong>Address:</strong>
                {orderDetail.shippingAddress.address},{" "}
                {orderDetail.shippingAddress.city}{" "}
                {orderDetail.shippingAddress.postalCode},{" "}
                {orderDetail.shippingAddress.country}
              </p>
              {orderDetail.isDelivered ? (
                <Message variant="success">
                  Delivered on {orderDetail.deliveredAt}
                </Message>
              ) : (
                <Message variant="danger">Not Delivered</Message>
              )}
            </ListGroup.Item>

            <ListGroup.Item>
              <h2>Payment Method</h2>
              <p>
                <strong>Method: </strong>
                {orderDetail.paymentMethod}
              </p>
              {orderDetail?.isPaid ? (
                <Message variant="success">
                  Paid on {formatDate(orderDetail?.transactionHistory?.paidAt)}
                </Message>
              ) : (
                <Message variant="danger">Not Paid</Message>
              )}
            </ListGroup.Item>

            <ListGroup.Item>
              <h2>Order Items</h2>
              {orderDetail.orderItems.length === 0 ? (
                <Message>Order is empty</Message>
              ) : (
                <ListGroup variant="flush">
                  {orderDetail.orderItems.map((item, index) => (
                    <ListGroup.Item key={index}>
                      <Row>
                        <Col md={1}>
                          <Image
                            src={item.image}
                            alt={item.name}
                            fluid
                            rounded
                          />
                        </Col>
                        <Col>
                          <Link to={`/product/${item.product}`}>
                            {item.name}
                          </Link>
                        </Col>
                        <Col md={4}>
                          {item.qty} x ${item.price} = ${item.qty * item.price}
                        </Col>
                      </Row>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              )}
            </ListGroup.Item>
          </ListGroup>
        </Col>
        <Col md={4}>
          <Card>
            <ListGroup variant="flush">
              <ListGroup.Item>
                <h2>Order Summary</h2>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Items</Col>
                  <Col>${orderDetail.itemsPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Shipping</Col>
                  <Col>${orderDetail.shippingPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Tax</Col>
                  <Col>${orderDetail.taxPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Total</Col>
                  <Col>${orderDetail.totalPrice}</Col>
                </Row>
              </ListGroup.Item>
              {!orderDetail.isPaid && (
                <ListGroup.Item>
                  <Button onClick={handleCheckOut} className="payment-btn">
                    CPay
                  </Button>
                </ListGroup.Item>
              )}
              {/* {loadingDeliver && <Loader />} */}
              {profile &&
                profile?.isAdmin &&
                orderDetail.isPaid &&
                !orderDetail.isDelivered && (
                  <ListGroup.Item>
                    <Button
                      type="button"
                      className="btn btn-block"
                      onClick={deliverHandler}
                    >
                      Mark As Delivered
                    </Button>
                  </ListGroup.Item>
                )}
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default OrderScreen;
