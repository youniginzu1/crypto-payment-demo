import React, { useMemo } from "react";
import { Button, Card, Col, Image, ListGroup, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

import { createOrder } from "../api/order";
import CheckoutSteps from "../components/CheckoutSteps";
import Message from "../components/Message";
import { addDecimals } from "../utils/helper";
import useCart from "../utils/hooks/useCart";

const PlaceOrderScreen = ({ history }) => {
  const { cartItems, shippingAddress, paymentMethod, removeAllItemsInCart } =
    useCart();

  const cartInfo = useMemo(() => {
    if (cartItems && shippingAddress && paymentMethod) {
      const itemsPrice = addDecimals(
        cartItems?.reduce((acc, item) => acc + item.price * item.qty, 0)
      );
      const shippingPrice = addDecimals(itemsPrice > 100 ? 0 : 100);
      const taxPrice = addDecimals(Number((0.15 * itemsPrice).toFixed(2)));

      return {
        cartItems,
        shippingAddress,
        paymentMethod,
        itemsPrice,
        shippingPrice,
        taxPrice,
        totalPrice: (
          Number(itemsPrice) +
          Number(shippingPrice) +
          Number(taxPrice)
        ).toFixed(2),
      };
    }
  }, [cartItems, shippingAddress, paymentMethod]);

  const placeOrderHandler = () => {
    createOrder({
      orderItems: cartInfo?.cartItems,
      shippingAddress: cartInfo?.shippingAddress,
      paymentMethod: cartInfo?.paymentMethod,
      itemsPrice: cartInfo?.itemsPrice,
      shippingPrice: cartInfo?.shippingPrice,
      taxPrice: cartInfo?.taxPrice,
      totalPrice: cartInfo?.totalPrice,
    }).then((data) => {
      removeAllItemsInCart();
      history.push(`/order/${data?._id}`);
    });
  };

  return (
    <>
      <CheckoutSteps step1 step2 step3 step4 />
      <Row>
        <Col md={8}>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <h2>Shipping</h2>
              <p>
                <strong>Address:</strong>
                {cartInfo?.shippingAddress.address},{" "}
                {cartInfo?.shippingAddress.city}{" "}
                {cartInfo?.shippingAddress.postalCode},{" "}
                {cartInfo?.shippingAddress.country}
              </p>
            </ListGroup.Item>

            <ListGroup.Item>
              <h2>Payment Method</h2>
              <strong>Method: </strong>
              {cartInfo?.paymentMethod}
            </ListGroup.Item>

            <ListGroup.Item>
              <h2>Order Items</h2>
              {cartInfo?.cartItems.length === 0 ? (
                <Message>Your cart is empty</Message>
              ) : (
                <ListGroup variant="flush">
                  {cartInfo?.cartItems.map((item, index) => (
                    <ListGroup.Item key={index}>
                      <Row>
                        <Col md={1}>
                          <Image
                            src={item.image}
                            alt={item.name}
                            fluid
                            rounded
                          />
                        </Col>
                        <Col>
                          <Link to={`/product/${item.product}`}>
                            {item.name}
                          </Link>
                        </Col>
                        <Col md={4}>
                          {item.qty} x ${item.price} = ${item.qty * item.price}
                        </Col>
                      </Row>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              )}
            </ListGroup.Item>
          </ListGroup>
        </Col>
        <Col md={4}>
          <Card>
            <ListGroup variant="flush">
              <ListGroup.Item>
                <h2>Order Summary</h2>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Items</Col>
                  <Col>${cartInfo?.itemsPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Shipping</Col>
                  <Col>${cartInfo?.shippingPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Tax</Col>
                  <Col>${cartInfo?.taxPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Total</Col>
                  <Col>${cartInfo?.totalPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Button
                  type="button"
                  className="btn-block"
                  disabled={cartInfo?.cartItems === 0}
                  onClick={placeOrderHandler}
                >
                  Place Order
                </Button>
              </ListGroup.Item>
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default PlaceOrderScreen;
