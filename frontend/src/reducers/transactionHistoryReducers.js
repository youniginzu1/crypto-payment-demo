import {
  TRANSACTION_HISTORY_LIST_MY_FAIL,
  TRANSACTION_HISTORY_LIST_MY_REQUEST,
  TRANSACTION_HISTORY_LIST_MY_RESET,
  TRANSACTION_HISTORY_LIST_MY_SUCCESS,
} from "../constants/transactionHistoryConstants";

export const transactionHistoriesMyReducer = (
  state = { transactionHistories: [] },
  action
) => {
  switch (action.type) {
    case TRANSACTION_HISTORY_LIST_MY_REQUEST:
      return {
        loading: true,
      };
    case TRANSACTION_HISTORY_LIST_MY_SUCCESS:
      return {
        loading: false,
        transactionHistories: action.payload,
      };
    case TRANSACTION_HISTORY_LIST_MY_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case TRANSACTION_HISTORY_LIST_MY_RESET:
      return { transactionHistories: [] };
    default:
      return state;
  }
};
