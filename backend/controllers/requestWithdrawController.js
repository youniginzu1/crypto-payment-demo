import asyncHandler from "express-async-handler";
import axios from "axios";

import RequestWithdraw from "../models/requestWithdrawModel.js";
import User from "../models/userModel.js";
import TransactionHistory from "../models/transactionHistoryModel.js";

// @desc    Get all request withdraw
// @route   GET /api/request-withdraw/list
const getRequestWithdrawList = asyncHandler(async (req, res) => {
  const requestWithdrawList = await RequestWithdraw.find({
    user: req?.user?._id,
  });
  res.json(requestWithdrawList);
});

const createRequestWithdraw = asyncHandler(async (req, res) => {
  const { amount, receiverAddress } = req.body;
  const user = await User.findOne({
    _id: req?.user?._id,
  });

  if (!amount || !receiverAddress) {
    res.status(400);
    throw new Error("Invalid input.");
  }
  if (user && user?.totalBonusPoint < Number(amount)) {
    res.status(400);
    throw new Error("Insufficient wallet balance.");
  }

  try {
    const paymentApiUrl = process.env.PAYMENT_API_URL;
    const requestWithdraw = new RequestWithdraw({
      amount,
      receiverAddress,
      user: req.user._id,
      status: 2,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    });

    const createdRequestWithdraw = await requestWithdraw.save();

    if (paymentApiUrl) {
      await axios.post(`${paymentApiUrl}/request/withdraw`, {
        amount,
        receiverAddress,
        requestClientId: createdRequestWithdraw?._id,
        clientId: user?._id,
      });
    }

    res.status(201).json(createdRequestWithdraw);
  } catch (err) {
    res.status(400);
    throw new Error("Unknown error.");
  }
});

const receiverWithdrawData = asyncHandler(async (req, res) => {
  const body = req?.body;
  if (!body?.requestClientId || typeof body?.requestStatus !== "number") {
    res.status(400);
    throw new Error("Invalid input.");
  }

  const updatedRequestWithdraw = await RequestWithdraw.findOneAndUpdate(
    {
      _id: body?.requestClientId,
    },
    {
      status: body?.requestStatus,
      updatedAt: Date.now(),
    }
  );
  const user = await User.findOne({
    _id: updatedRequestWithdraw?.user,
  });

  if (body?.requestStatus === 1) {
    const transactionHistory = new TransactionHistory({
      user: updatedRequestWithdraw?.user,
      paymentMethod: "CRYPTO",
      paymentResult: null,
      type: 2,
      paidAt: Date.now(),
      amount: updatedRequestWithdraw?.amount,
      requestWithdraw: updatedRequestWithdraw?._id,
    });
    await transactionHistory.save();

    user.totalBonusPoint -= updatedRequestWithdraw?.amount;
    await user.save();
  }

  return updatedRequestWithdraw;
});

export { getRequestWithdrawList, createRequestWithdraw, receiverWithdrawData };
