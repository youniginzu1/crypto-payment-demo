import asyncHandler from "express-async-handler";
import TransactionHistory from "../models/transactionHistoryModel.js";
import User from "../models/userModel.js";

// @desc    Get all transaction history
// @route   GET /api/transaction-histories
const getTransactionHistories = asyncHandler(async (req, res) => {
  const transactionHistories = await TransactionHistory.find({
    user: req?.user?._id,
  });
  res.json(transactionHistories);
});

const calculateBonusPoint = asyncHandler(async (req, res) => {
  const transactions = await TransactionHistory.find({});
  const bonusPercent = Number(process.env.BONUS_PERCENT) || 0;

  for (const transaction of transactions) {
    const amount = transaction?.amount;
    const bonusPoint = (bonusPercent / 100) * amount;
    const userId = transaction?.user;
    transaction.bonusPoint = bonusPoint;

    await transaction.save();
    await User.updateOne(
      { _id: userId },
      {
        $inc: {
          totalBonusPoint: bonusPoint,
        },
      }
    );
  }

  res.json({
    status: "Success",
  });
});

export { getTransactionHistories, calculateBonusPoint };
