import mongoose from "mongoose";

const requestWithdrawSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    receiverAddress: {
      type: String,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
      default: 0.0,
    },
    status: {
      type: Number,
      required: true,
      default: 2, //2: Pending, 1: Success, 0: Failed
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  {
    timestamps: true,
  }
);

const RequestWithdraw = mongoose.model(
  "RequestWithdraw",
  requestWithdrawSchema
);

export default RequestWithdraw;
