import mongoose from "mongoose";

const transactionHistorySchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    paymentMethod: {
      type: String,
      required: true,
    },
    paymentResult: {
      id: { type: String },
      status: { type: String },
      update_time: { type: String },
      email_address: { type: String },
    },
    amount: {
      type: Number,
      required: true,
      default: 0.0,
    },
    bonusPoint: {
      type: Number,
      required: false,
      default: 0.0,
    },
    type: {
      type: Number,
      required: true,
      default: 1, //1: Payment order, 2: Withdraw
    },
    paidAt: {
      type: Date,
    },
    order: {
      type: mongoose.Schema.Types.ObjectId,
      required: false,
      ref: "Order",
    },
    requestWithdraw: {
      type: mongoose.Schema.Types.ObjectId,
      required: false,
      ref: "Request",
    },
  },
  {
    timestamps: true,
  }
);

const TransactionHistory = mongoose.model(
  "TransactionHistory",
  transactionHistorySchema
);

export default TransactionHistory;
