import express from "express";
import { protect } from "../middleware/authMiddleware.js";
import {
  createRequestWithdraw,
  getRequestWithdrawList,
  receiverWithdrawData,
} from "../controllers/requestWithdrawController.js";
const router = express.Router();

router.route("/list").get(protect, getRequestWithdrawList);
router.route("/").post(protect, createRequestWithdraw);
router.route("/complete").post(receiverWithdrawData);

export default router;
