import express from "express";
import {
  getTransactionHistories,
  calculateBonusPoint,
} from "../controllers/transactionHistoryController.js";
import { protect } from "../middleware/authMiddleware.js";
const router = express.Router();

router.route("/").get(protect, getTransactionHistories);
router.route("/calculate-bonus-point").get(calculateBonusPoint);

export default router;
